import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.awt.dnd.DragGestureEvent;
import java.net.URL;
import java.net.MalformedURLException;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestClass {
    protected WebDriver driver;

    DesiredCapabilities dc = new DesiredCapabilities();

    //String memberPhoneNumber = "6472092199";
    //String memberPhoneNumber = "6472092197";
    String memberPhoneNumber = "6472092191";
    String memberName = "Chun Yu";
    String memberContactNumber = "123456";
    String creditToAdd = "500";
    String singlePrice = "80";
    String trainerPercentage = "50";
    String trainerName = "Charlie Yu";
    String comment = "Two month's membership 500.";


    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("deviceVersion", "8.1");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "com.inventif");
        dc.setCapability("appActivity", "com.inventif.MainActivity");

        dc.setCapability("unicodeKeyboard", true);
        dc.setCapability("resetKeyboard", true);

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
    }

    @Test
    public void addNewTrainers() throws Exception {
        Thread.sleep(3000);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@text='Continue ']")).click();

        Thread.sleep(2000);

        int x = 368;
        int y = 356;
        TouchAction action = new TouchAction((AndroidDriver)driver);
        action.press(x, y).release().perform();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='Please enter your username']")).click();
        driver.findElement(By.xpath("//*[@text='Please enter your username']")).sendKeys("1");
        driver.findElement(By.xpath("//*[@text='Please enter your password']")).click();
        driver.findElement(By.xpath("//*[@text='Please enter your password']")).sendKeys("1");
        driver.findElement(By.xpath("//*[@text='Submit']")).click();
        driver.findElement(By.xpath("//*[@text='Submit']")).click();

        /*By trainersBtn = By.xpath("//*[@text='Trainers']");
        waitForVisibilityOf(trainersBtn);
        driver.findElement(trainersBtn).click();

        driver.findElement(By.xpath("//*[@text='New']")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text=concat('please enter member', \"'\", 's phone number')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('please enter member', \"'\", 's phone number')]")).sendKeys("6472092199");

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter member name']")).click();
        driver.findElement(By.xpath("//*[@text='please enter member name']")).sendKeys("Charlie Yu");

        driver.findElement(By.xpath("//*[@text='Supervisor']")).click();
        driver.findElement(By.xpath("//*[@text='Supervisor']")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='Active']")).click();

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='Save']")).click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@text='OK']")).click();

        driver.navigate().back();*/

        By memberBtn = By.xpath("//*[@text='Members']");
        waitForVisibilityOf(memberBtn);

        driver.findElement(memberBtn).click();
        driver.findElement(By.xpath("//*[@text='New']")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text=concat('please enter member', \"'\", 's phone number')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('please enter member', \"'\", 's phone number')]")).sendKeys(memberPhoneNumber);

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter member name']")).click();
        driver.findElement(By.xpath("//*[@text='please enter member name']")).sendKeys(memberName);

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter contract number']")).click();
        driver.findElement(By.xpath("//*[@text='please enter contract number']")).sendKeys(memberContactNumber);

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter the new amount']")).click();
        driver.findElement(By.xpath("//*[@text='please enter the new amount']")).sendKeys(creditToAdd);

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter the price for each session']")).click();
        driver.findElement(By.xpath("//*[@text='please enter the price for each session']")).sendKeys(singlePrice);

        Thread.sleep(1000);

        //For scrolling, can move as per you screen size
        swipeVertical((AndroidDriver)driver,0.9,0.1,0.5,3000);
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='select date']")).click();
        driver.findElement(By.xpath("//*[@text='select date']")).click();
        driver.findElement(By.xpath("//*[@text='OK']")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='select date']")).click();
        Thread.sleep(1000);

        driver.findElement(By.id("next")).click();
        Thread.sleep(1000);

        driver.findElement(By.id("next")).click();
        Thread.sleep(1000);

        //For taking current date and separating day
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        System.out.println("Day: "+day);

        By currentDay = By.xpath("//*[@text='"+day+"']");

        driver.findElement(currentDay).click();
        driver.findElement(By.xpath("//*[@text='OK']")).click();

        //For scrolling, can move as per you screen size
        swipeVertical((AndroidDriver)driver,0.9,0.2,0.5,3000);
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@text='please enter the Trainer percentage']")).click();
        driver.findElement(By.xpath("//*[@text='please enter the Trainer percentage']")).sendKeys(trainerPercentage);

        driver.findElement(By.xpath("//*[@text='Please select trainer']")).click();
        Thread.sleep(500);

        By selectTrainer = By.xpath("//*[@text='"+trainerName+"']");
        driver.findElement(selectTrainer).click();

        driver.findElement(By.xpath("//*[@text='Partial payment']")).click();
        driver.findElement(By.xpath("//*[@text='Partial payment']")).click();
        driver.findElement(By.xpath("//*[@text='Write comment here...']")).click();
        driver.findElement(By.xpath("//*[@text='Write comment here...']")).sendKeys(comment);
        Thread.sleep(500);

        driver.findElement(By.xpath("//*[@text='Save changes']")).click();
        driver.findElement(By.xpath("//*[@text='Save changes']")).click();




    }


    @AfterMethod
    public void tearDown() {
        //driver.quit();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(anchor, startPoint).waitAction(Duration.ofMillis(duration)).moveTo(anchor, endPoint).release().perform();
    }
}